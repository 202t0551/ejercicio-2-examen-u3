public class Equipo
{
    private int numUniforme; 
    private String nombre;
    private String posicion;
    
    public Equipo(int numUniforme, String nombre, String posicion){
    this.numUniforme = numUniforme;
    this.nombre = nombre;
    this.posicion = posicion;
    }
    
    
    //Metodos get
    public int getNumUniforme(){
    return numUniforme;
    }
    
    public String getNombre(){
    return nombre;
    }
    
    public String getPosicion(){
    return posicion;
    }
    
    //Metodos Set
    public void setNumUniforme(int numUniforme){
    this.numUniforme = numUniforme;
    }
    
    
    public void setNombre(String nombre){
    this.nombre = nombre;
    }
        
    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }
}
