public class Bateador extends Equipo
{
    private int hits;
    public Bateador(int numUniforme, String nombre, String posicion,int hits) {
        super(numUniforme, nombre, posicion);
        this.hits = hits;
    }
    
    public int getHits(){
      return hits;
    }
    
    public void setHits(int hits){
      this.hits = hits;
    }
    
    public void mostrar(){
        System.out.println("Numero del uniforme: " + getNumUniforme());
        System.out.println("Nombre del jugador: " + getNombre());
        System.out.println("Posicion: " + getPosicion());
        System.out.println("Hits: " + getHits());              
    }
}