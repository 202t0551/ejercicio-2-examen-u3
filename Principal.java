import java.util.Scanner;
public class Principal
{
     public static void main(String[] args) {
        int i=0;
        
        Pitcher p1 = new Pitcher(40,"Jose","Pitcher",5);
        Bateador b1 = new Bateador(15,"Carl","Bateador",8);
        JPosicion jp1 = new JPosicion(85,"Thomas","Jugador de Posicion",3);


        //Nos muestra los detalles del objeto
        System.out.println("-------------------------------");
        p1.mostrar();        
        System.out.println("-------------------------------");
        b1.mostrar();        
        System.out.println("-------------------------------");
        jp1.mostrar();        
        System.out.println("-------------------------------");
    }   
}
