public class JPosicion extends Equipo
{
    private int errores;
    public JPosicion(int numUniforme, String nombre, String posicion,int errores) {
        super(numUniforme, nombre, posicion);
        this.errores = errores;
    }
    
    public int getErrores(){
      return errores;
    }
    
    public void setErrores(int errores){
      this.errores = errores;
    }
    
    public void mostrar(){
        System.out.println("Numero del uniforme: " + getNumUniforme());
        System.out.println("Nombre del jugador: " + getNombre());
        System.out.println("Posicion: " + getPosicion());
        System.out.println("Errores cometidos: " + getErrores());              
    }
}