public class Pitcher extends Equipo
{
    private int ponchados;
    public Pitcher(int numUniforme, String nombre, String posicion,int ponchados) {
        super(numUniforme, nombre, posicion);
        this.ponchados = ponchados;
    }
    
    public int getPonchados(){
      return ponchados;
    }
    
    public void setPonchados(int ponchados){
      this.ponchados = ponchados;
    }
    
    public void mostrar(){
        System.out.println("Numero del uniforme: " + getNumUniforme());
        System.out.println("Nombre del jugador: " + getNombre());
        System.out.println("Posicion: " + getPosicion());
        System.out.println("Ponchados: " + getPonchados());              
    }
}